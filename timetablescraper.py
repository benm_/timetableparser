import os
import glob
import sqlite3
import re
from xlrd import open_workbook

RESDIR = 'res'
COURSES = {}
ACTIVITIES = []
VENUES = {}
BUILDINGS = {}
ACTIVITY_TYPES = {}
DAYMAP = {'MONDAY':0, 'TUESDAY':1, 'WEDNESDAY':2, 'THURSDAY':3, 'FRIDAY':4, 'SATURDAY':5, 'SUNDAY':6}
SEMMAP = {'whole year':0, 'first':1, 'second':2}

def gofiles():
    resfiles = os.listdir(RESDIR)
    for f in resfiles:
        if len(f)>6:
            if f[-4:] == '.xls':
                workonfile(os.path.join(RESDIR, f))
    print('COURSES: %d' % len(COURSES))
    print('ACTIVITIES: %d' % len(ACTIVITIES))
    print('BUILDINGS: %d' % len(BUILDINGS))
    print('VENUES: %d' % len(VENUES))
    print('ACTIVITY_TYPES: %d' % len(ACTIVITY_TYPES))
    # print_courses()
    push_to_sql()

def workonfile(filename):
    wb = open_workbook(filename)
    for sheet in wb.sheets():
        for row in range(1,sheet.nrows):
            
            # detect course code
            courseS = sheet.cell(row,0).value.strip()

            # if course not created yet
            if not courseS in COURSES:                
                courseDescription = sheet.cell(row,1).value
                COURSES[courseS] = Course(len(COURSES), courseS, courseDescription)
            course_id = COURSES[courseS].id

            # detect activity on this line
            actypeS = sheet.cell(row, 3).value

            if not actypeS in ACTIVITY_TYPES:
                ACTIVITY_TYPES[actypeS] = ActivityType(len(ACTIVITY_TYPES), actypeS)
            actype_id = ACTIVITY_TYPES[actypeS].id
            
            day = DAYMAP[sheet.cell(row,4).value]                   # day as 0=monday to 6=sunday
            time = time_to_minutes(sheet.cell(row, 5).value)        # time of day 
            duration = time_to_minutes(sheet.cell(row, 6).value)    # length of lectures in minutes
            
            venueS = sheet.cell(row, 7).value
            
            m = re.search('([a-zA-Z]*)', venueS)
            building_code = m.group(0)

            if not building_code in BUILDINGS:
                BUILDINGS[building_code] = Building(len(BUILDINGS), building_code, "", 0, 0)            
            bid = BUILDINGS[building_code].id
            
            if not venueS in VENUES:
                VENUES[venueS] = Venue(len(VENUES), bid, venueS)                
            venue_id = VENUES[venueS].id
                        
            semS = sheet.cell(row, 11).value.lower()
            sem = SEMMAP[semS]

            # create activity
            activity = Activity(len(ACTIVITIES), course_id, venue_id, actype_id, day, time, duration, sem)
            ACTIVITIES.append(activity)

def print_courses():
    for i, key in enumerate(sorted(COURSES.keys())):
        course = COURSES[key]
        print(str(i) + '. ' + course.code + ' '  + str(len(course.activities)))

def time_to_minutes(string):
    h = int(string.split(':')[0])
    m = int(string.split(':')[1])
    return h*60 + m


def sql_droptables(connection):
    connection.execute("DROP TABLE IF EXISTS courses;")
    connection.execute("DROP TABLE IF EXISTS venues;")
    connection.execute("DROP TABLE IF EXISTS activities;")
    connection.execute("DROP TABLE IF EXISTS activitytypes")
    connection.execute("DROP TABLE IF EXISTS buildings;")
    connection.execute("DROP TABLE IF EXISTS android_metadata")

def sql_createtables(connection):
    connection.execute(Course.schema)
    connection.execute(Venue.schema)
    connection.execute(Activity.schema)
    connection.execute(ActivityType.schema)
    connection.execute(Building.schema)
    connection.execute(AndroidSchema.schema)

def push_to_sql():
    conn = sqlite3.connect('timetable.db')
    sql_droptables(conn)
    sql_createtables(conn)

    cur = conn.cursor()

    # VENUES
    sql = "INSERT INTO venues VALUES (?, ?, ?);"
    for k, v in VENUES.iteritems():
        cur.execute(sql, (v.id, v.building_id, v.name))

    conn.commit()

    # BUILDINGS    
    sql = "INSERT INTO buildings VALUES (?, ?, ?, ?, ?);"    
    for k, v in BUILDINGS.iteritems():
        cur.execute(sql,(v.id, v.code, v.name, v.latitude, v.longitude))
        
    conn.commit()

    # ACTIVITY TYPES
    sql = "INSERT INTO activitytypes VALUES (?, ?);"
    for k, v in ACTIVITY_TYPES.iteritems():
        cur.execute(sql,(v.id, v.name))

    conn.commit()

    # COURSES
    sql = "INSERT INTO courses VALUES (?, ?, ?);"
    for k, v in COURSES.iteritems():
        cur.execute(sql,(v.id, v.code, v.name))

    # ACTIVITIES
    sql = "INSERT INTO activities VALUES (?, ?, ?, ?, ?, ?, ?, ?);"
    for a in ACTIVITIES:        
        cur.execute(sql,(a.id, a.course_id, a.venue_id, a.activity_type_id, a.day, a.time, a.duration, a.semester))
   
    conn.commit()
    conn.close()
    

class Course:
    schema = '''CREATE TABLE IF NOT EXISTS "courses" (
                "_id" INTEGER PRIMARY KEY,                
                "code" TEXT,
                "name" TEXT
            );'''
    
    def __init__(self, idn, code, name):
        self.id = idn
        self.code = code
        self.name = name

class Activity:
    schema = '''CREATE TABLE IF NOT EXISTS "activities" (
                            "_id" INTEGER PRIMARY KEY,
                            "course_id" INTEGER,
                            "venue_id" INTEGER,
                            "activity_type_id" INTEGER,
                            "day" INTEGER,
                            "time" INTEGER,
                            "duration" INTEGER,
                            "semester" INTEGER
                        );'''
    
    def __init__(self, idn, course_id, venue_id, acttype_id, day, time, duration, semester):
        self.id = idn
        self.course_id = course_id        
        self.venue_id = venue_id
        self.activity_type_id = acttype_id
        self.day = day
        self.time = time
        self.duration = duration
        self.semester = semester

class ActivityType:
    schema = '''CREATE TABLE IF NOT EXISTS "activitytypes" (
                            "_id" INTEGER PRIMARY KEY,
                            "name" TEXT
                        );'''
    
    def __init__(self, idn, name):
        self.id = idn
        self.name = name

class Building:
    schema = '''CREATE TABLE IF NOT EXISTS "buildings" (
                            "_id" INTEGER PRIMARY KEY,
                            "code" TEXT,
                            "name" TEXT,
                            "latitude" REAL,
                            "longitude" REAL
                        );'''
    
    def __init__(self, idn, code, name="", latitude=0, longitude=0):        
        self.id = idn
        self.code = code
        self.name = name
        self.latitude = latitude
        self.longitude = longitude

class Venue:
    schema = '''CREATE TABLE IF NOT EXISTS "venues" (
                            "_id" INTEGER PRIMARY KEY,
                            "building_id" INTEGER,
                            "name" TEXT
                        );'''
    
    def __init__(self, idn, bid, name):
        self.id = idn
        self.building_id = bid
        self.name = name

class AndroidSchema:
    schema = '''CREATE TABLE "android_metadata" ("locale" TEXT DEFAULT 'en_US');'''

        
if __name__ == '__main__':
    gofiles()
