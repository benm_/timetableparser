PYTHON CODE TO PARSE TIMETABLE DATA FROM XLS FILES
--------------------------------------------------

modules used:
xlrd-0.8.0
	$ python setup.py install

Save all xls timetable files into the 'res' folder. timetablescraper.py will automatically pull them from there.
Note, timetablescraper.py assumes all data is valid.
Note, times are stored in the database as minutes past midnight.
Note, week days are monday=0 sunday=6
