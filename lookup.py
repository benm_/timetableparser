import sqlite3

def print_statistics():
    conn = sqlite3.connect('timetable.db')

    cur = conn.cursor()

    cur.execute("SELECT COUNT(*) FROM courses")
    num_courses = cur.fetchone()[0]

    cur.execute("SELECT COUNT(*) FROM activities")
    num_lectures = cur.fetchone()[0]

    cur.execute("SELECT COUNT(*) FROM venues")
    num_venues = cur.fetchone()[0]

    print('-- STATISTICS --------------------------------------')
    print(str(num_courses) + ' Courses with ' + str(num_lectures) + ' Lectures  in ' + str(num_venues) + ' Venues.')
    print('----------------------------------------------------')

    conn.close()

def lookup_course(course_code):
    conn = sqlite3.connect('timetable.db')

    cur = conn.cursor()

    cur.execute("SELECT * FROM courses WHERE code LIKE ?", (str('%'+course_code+'%'),))
    matching_courses = cur.fetchall()
    print(str(len(matching_courses)) + " results for '"+course_code+"':")
    for match in matching_courses:
        print(match[2] + '\t' + match[1])
    
    conn.close()

def lookup_venue(venue_name):
    conn = sqlite3.connect('timetable.db')

    cur = conn.cursor()

    cur.execute("SELECT * FROM buildings")
    buildings = cur.fetchall()    
    for b in buildings:
        cur.execute("SELECT * FROM venues WHERE building_id=?", (str(b[0]),))
        print(b[1] + "\t\t" + str(len(cur.fetchall())))

    
    conn.close()




if __name__ == '__main__':
    print_statistics()
    #lookup_course('CS')
    print()
    lookup_venue('')
